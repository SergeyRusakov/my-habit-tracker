import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HabitModule } from './habit/habit.module';
import { Habit } from './habit/habit.enitity';
import { GoalModule } from './goal/goal.module';
import { Goal } from './goal/goal.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "postgres",
      host: "localhost",
      port: 5432,
      username: "postgres",
      password: "postgres",
      database: "habit_tracker",
      entities: [
        Goal,
        Habit,
      ],
      synchronize: true
    }),
    HabitModule,
    GoalModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
