import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { HabitService } from './habit.service';
import { Habit } from './habit.enitity';

@Controller('habit')
export class HabitController {

  constructor(private service: HabitService) {
  }

  @Get()
  public getAll(): Promise<Habit[]> {
    return this.service.findAll();
  }

  @Get(':uuid')
  public getById(@Param('uuid') uuid: string): Promise<Habit> {
    return this.service.findOne(uuid);
  }

  @Post()
  public post(@Body() habit: Habit): Promise<Habit> {
    return this.service.createOrUpdate(habit);
  }

  @Put()
  public put(@Body() habit: Habit): Promise<Habit> {
    return this.service.createOrUpdate(habit);
  }

  @Delete(':uuid')
  public delete(@Param('uuid') uuid: string): Promise<void> {
    return this.service.remove(uuid);
  }

}
