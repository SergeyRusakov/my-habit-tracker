import { PrimaryGeneratedColumn, Column, Entity, CreateDateColumn, ManyToOne } from 'typeorm';
import { Goal } from '../goal/goal.entity';

@Entity('habits')
export class Habit {

  @PrimaryGeneratedColumn('uuid')
  public uuid: string;
  @Column()
  public description: string;
  @CreateDateColumn()
  public created: Date;
  @ManyToOne(() => Goal)
  public goal: Goal;

}
