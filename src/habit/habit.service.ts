import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Habit } from './habit.enitity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class HabitService {

  constructor(
    @InjectRepository(Habit) private repository: Repository<Habit>,
  ) {
  }

  public findOne(uuid: string): Promise<Habit> {
    return this.repository.findOne(uuid);
  }

  public findAll(): Promise<Habit[]> {
    return this.repository.find();
  }

  public createOrUpdate(habit: Habit): Promise<Habit> {
    return this.repository.save(habit);
  }

  public async remove(uuid: string): Promise<void> {
    await this.repository.delete(uuid);
  }

}
