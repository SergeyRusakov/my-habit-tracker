import { Module } from '@nestjs/common';
import { Habit } from './habit.enitity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HabitController } from './habit.controller';
import { HabitService } from './habit.service';

@Module({
  imports: [TypeOrmModule.forFeature([Habit])],
  exports: [TypeOrmModule],
  controllers: [HabitController],
  providers: [HabitService]
})
export class HabitModule {}
