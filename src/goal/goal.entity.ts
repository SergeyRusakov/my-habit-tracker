import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Habit } from '../habit/habit.enitity';
@Entity('goals')
export class Goal {

  @PrimaryGeneratedColumn('uuid')
  public uuid: string;
  @Column()
  public description: string;
  @CreateDateColumn()
  public created: Date;
  @OneToMany(
    () => Habit,
      habit => habit.goal,
    {
      orphanedRowAction: 'delete',
      eager: false,
    }
  )
  public habits: Habit[];

}