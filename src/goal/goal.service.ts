import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Goal } from './goal.entity';

@Injectable()
export class GoalService {

  constructor(
    @InjectRepository(Goal) private repository: Repository<Goal>,
  ) {
  }

  public findOne(uuid: string): Promise<Goal> {
    return this.repository.findOne(uuid);
  }

  public findAll(): Promise<Goal[]> {
    return this.repository.find();
  }

  public createOrUpdate(Goal: Goal): Promise<Goal> {
    return this.repository.save(Goal);
  }

  public async remove(uuid: string): Promise<void> {
    await this.repository.delete(uuid);
  }

}
