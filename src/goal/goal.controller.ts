import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { GoalService } from './goal.service';
import { Goal } from './goal.entity';

@Controller('goal')
export class GoalController {

  constructor(private service: GoalService) {
  }

  @Get()
  public getAll(): Promise<Goal[]> {
    return this.service.findAll();
  }

  @Get(':uuid')
  public getById(@Param('uuid') uuid: string): Promise<Goal> {
    return this.service.findOne(uuid);
  }

  @Post()
  public post(@Body() Goal: Goal): Promise<Goal> {
    return this.service.createOrUpdate(Goal);
  }

  @Put()
  public put(@Body() Goal: Goal): Promise<Goal> {
    return this.service.createOrUpdate(Goal);
  }

  @Delete(':uuid')
  public delete(@Param('uuid') uuid: string): Promise<void> {
    return this.service.remove(uuid);
  }
}
